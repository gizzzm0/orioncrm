<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CRM_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        /*if(!$this->input->is_ajax_request()){
            $this->output->enable_profiler(TRUE);
        }*/

        /**
         * Fix for users who don't replace all files during update !!!
         */
        if (!class_exists('ForceUTF8\Encoding') && file_exists(APPPATH . 'vendor/autoload.php')) {
            require_once(APPPATH . 'vendor/autoload.php');
        }

        if (is_dir(FCPATH . 'install') && ENVIRONMENT != 'development') {
            if(!$this->rename_install_dir()) {
                echo '<h3>Couldn\'t Rename the install folder. Please Delete or Rename it manually.</h3>';
                die;
            }
        }

        $this->db->reconnect();
        $timezone = get_option('default_timezone');
        if ($timezone != '') {
            date_default_timezone_set($timezone);
        }

        do_action('app_init');
    }

    private function rename_install_dir()
    {
        if (rename(realpath('install'), 'install.bak/') == true) {
            return true;
        }

        return false;
    }
}
